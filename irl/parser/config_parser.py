import importlib
import json
from typing import List, Any, Dict, Union

from irl.data.data_manipulator.base import TickerDataManipulator, GeneralDataManipulator
from irl.data.data_provider.base import TickerDataProvider, GeneralDataProvider
from irl.env.stock_exchange import StockExchangeEnv


class ConfigParser:
    def __init__(self,
                 config_path: str):
        """
        :param constants_path: full path of the json constants file
        :param config_path: full path of the json config file
        """
        def populate_config_with_constants(config: Dict[str, Any],
                                           constants: Dict[str, Any]):
            for k, v in config.items():
                if isinstance(v, dict):
                    populate_config_with_constants(v, constants)

                if isinstance(v, list):
                    for elem in v:
                        if isinstance(elem, dict):
                            populate_config_with_constants(elem, constants)

                if k in constants:
                    config[k] = constants[k]
            return config

        config = json.load(open(config_path, 'r'))
        self.config = populate_config_with_constants(config['config'], config['constants'])

        self.data_providers = self.instantiate_modules(self.config['DataProviders'])
        self.data_manipulators = self.instantiate_modules(self.config['DataManipulators'])
        self.ticker_data_combinator = self.instantiate_modules(self.config['TickerDataCombinator'])
        self.general_data_combinator = self.instantiate_modules(self.config['GeneralDataCombinator'])

        self.config['DataAggregator']['kwargs']['checks'] = self.instantiate_modules(
            self.config['DataAggregator']['kwargs']['checks']
        )
        self.data_aggregator = self.instantiate_modules(self.config['DataAggregator'])

        self.initialise_data()

    def initalise_env(self,
                      output_dir: str,
                      validation: bool) -> StockExchangeEnv:
        self.config['Portfolio']['kwargs']['output_dir'] = output_dir
        env = StockExchangeEnv(validation,
                               self.data_aggregator,
                               self.instantiate_modules(self.config['Portfolio']))
        return env

    def initialise_data(self):
        for data_provider in self.data_providers:
            data_provider.get_data()

        self.ticker_data_combinator.combine([data_provider for data_provider in self.data_providers
                                             if isinstance(data_provider, TickerDataProvider)])
        self.general_data_combinator.combine([data_provider for data_provider in self.data_providers
                                              if isinstance(data_provider, GeneralDataProvider)])

        for data_manipulator in self.data_manipulators:
            if isinstance(data_manipulator, TickerDataManipulator):
                data_manipulator.manipulate(self.ticker_data_combinator)
            elif isinstance(data_manipulator, GeneralDataManipulator):
                data_manipulator.manipulate(self.general_data_combinator)

        self.data_aggregator.aggregate(self.ticker_data_combinator,
                                       self.general_data_combinator)

    def instantiate(self, module_dict: Dict) -> Any:
        module_name = '.'.join(module_dict['module'].split('.')[:-1])
        module = importlib.import_module(module_name)
        class_name = module_dict['module'].split('.')[-1]
        class_ = getattr(module, class_name)
        return class_(**module_dict['kwargs'])

    def instantiate_modules(self, modules: Union[List[Dict[str, Any]], Dict[str, Any]]) -> Union[List[Any], Any]:
        """
        imports and instantiates objects with kwargs

        :param modules:
        :return: list of instantiated objects of the same type
        """
        if isinstance(modules, list):
            return [self.instantiate(module) for module in modules]
        else:
            return self.instantiate(modules)
