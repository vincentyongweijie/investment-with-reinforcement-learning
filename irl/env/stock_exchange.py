import numpy as np
# import pandas as pd

import gym
from gym import spaces

from irl.data.data_aggregator.base import BaseDataAggregator
from irl.portfolio.portfolio import Portfolio


class StockExchangeEnv(gym.Env):
    def __init__(self,
                 validation: bool,
                 data_aggregator: BaseDataAggregator,
                 portfolio: Portfolio):
        """

        :param data: aggregated and normalised data with shape
                     [date, period, num_instruments*num_ticker_fields + general_fields]
        :param portfolio: portfolio instantiated
        """
        self.dates = data_aggregator.val_open_prices.index if validation else data_aggregator.open_prices.index
        self.date_idx = 0
        self.max_date_idx = len(self.dates)

        self.open_prices = data_aggregator.val_open_prices.values \
            if validation \
            else data_aggregator.open_prices.values
        self.close_prices = data_aggregator.val_close_prices.values \
            if validation \
            else data_aggregator.close_prices.values
        self.data = data_aggregator.val_data if validation else data_aggregator.data

        self.portfolio = portfolio
        # self.action_space = spaces.Box(low=-1.0,
        #                                high=1.0,
        #                                shape=(portfolio.num_instruments-1,),
        #                                dtype=np.float32)
        self.action_space = spaces.MultiDiscrete([portfolio.max_power * 2 + 1] * (portfolio.num_instruments - 1))
        self.observation_space = spaces.Box(low=-1.0,
                                            high=1.0,
                                            shape=(portfolio.period,
                                                   self.data.shape[2] + portfolio.num_instruments),
                                            dtype=np.float32)

    def step(self, action: np.ndarray):
        self.portfolio.execute(self.dates[self.date_idx],
                               action,
                               self.open_prices[self.date_idx],
                               self.close_prices[self.date_idx])

        next_state = self.data[self.date_idx]
        observation = np.concatenate((next_state, self.portfolio.state), axis=-1)
        self.date_idx += 1

        reward = self.portfolio.reward

        done = self.date_idx == self.max_date_idx

        return observation, reward, done, {}

    def reset(self):
        self.date_idx = 0
        initial_state = self.data[self.date_idx]
        portfolio_state = self.portfolio.reset()
        observation = np.concatenate((initial_state, portfolio_state), axis=-1)
        return observation
