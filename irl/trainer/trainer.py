import os

import numpy as np
from stable_baselines.common.policies import MlpLstmPolicy
from stable_baselines.common.vec_env import SubprocVecEnv
from stable_baselines import PPO2

from irl.env.stock_exchange import StockExchangeEnv
from irl.parser.config_parser import ConfigParser
from irl.portfolio.portfolio import Portfolio


class Trainer:
    def __init__(self,
                 num_envs: int,
                 config_path: str,
                 output_dir: str,
                 checkpoint: str = None):

        self.num_envs = num_envs
        self.output_dir = output_dir

        self.parser = ConfigParser(config_path)
        env_fns = [lambda env_num=env_num:
                   self.parser.initalise_env(os.path.join(self.output_dir, 'env_' + str(env_num+1)), validation=False)
                   for env_num in range(self.num_envs)]
        self.env = SubprocVecEnv(env_fns)

        if checkpoint:
            self.model = PPO2.load(checkpoint,
                                   self.env,
                                   verbose=1,
                                   tensorboard_log=output_dir + '_tensorboard_logs')
        else:
            self.model = PPO2(MlpLstmPolicy,
                              self.env,
                              verbose=1,
                              tensorboard_log=output_dir + '_tensorboard_logs')

    def train(self,
              total_timesteps: int) -> None:
        """
        trains agent

        :param totaL_timesteps: timesteps to train model
        :param output_path: directory to save model
        """
        os.mkdir(self.output_dir)
        for i in range(self.num_envs):
            os.mkdir(os.path.join(self.output_dir, 'env_' + str(i + 1)))
        os.mkdir(self.output_dir + '_tensorboard_logs')

        self.model.learn(total_timesteps=total_timesteps, tb_log_name="ppo2")
        self.model.save(os.path.join(self.output_dir, 'model'))

    def validate(self):
        env = self.parser.initalise_env(os.path.join(self.output_dir), validation=True)
        portfolio = self.evaluate_agent(env)
        portfolio.history.to_csv(os.path.join(self.output_dir, 'validation.csv'))

    def evaluate_agent(self, env: StockExchangeEnv) -> Portfolio:
        """
        evaluates agent

        :param model_path: model to be loaded
        """
        obs = env.reset()
        obs = np.tile(obs, (self.num_envs, 1, 1))
        done = False
        while not done:
            action, _states = self.model.predict(obs, deterministic=True)
            obs, rewards, done, info = env.step(action[0])

            obs = np.expand_dims(obs, axis=0)
            obs = np.tile(obs, (self.num_envs, 1, 1))
        return env.portfolio


if __name__ == '__main__':
    # HOLD
    # GOOG 5.55,
    # AMD 9.40,
    # AAPL 20.4
    # average 11.78

    # sector
    # average 0.432 from 2015
    output_dir = 'expt1/dailyrewards_sci_macd'
    config_path = './irl/tests/resources/parser/sector_config.json'
    trainer = Trainer(4,
                      config_path,
                      output_dir)
    trainer.train(5000000)
    trainer.validate()
