from abc import abstractmethod, ABCMeta
from collections import OrderedDict
from functools import reduce
from typing import List, Union

import pandas as pd

from irl.data.data_provider.base import TickerDataProvider, GeneralDataProvider


class BaseDataCombinator(metaclass=ABCMeta):
    """
    base class to combine data across multiple data providers
    """
    def __init__(self):
        pass

    @abstractmethod
    def combine(self,
                data_providers: Union[List[TickerDataProvider], List[GeneralDataProvider]]) -> None:
        """
        to be implemented by sub-classes

        sub-classes decide how to concatenate data across tickers
        """


class TickerDataCombinator(BaseDataCombinator):
    def __init__(self):
        super().__init__()
        self.data = OrderedDict()

    def combine(self, data_providers: List[TickerDataProvider]) -> None:
        if not data_providers:
            return

        tickers = data_providers[0].data.keys()
        for ticker in tickers:
            self.data[ticker] = reduce(lambda left, right: pd.merge(left, right, left_index=True, right_index=True),
                                       [data_provider.data[ticker] for data_provider in data_providers])


class GeneralDataCombinator(BaseDataCombinator):
    def __init__(self):
        super().__init__()
        self.data = None

    def combine(self, data_providers: List[GeneralDataProvider]) -> None:
        if not data_providers:
            return

        self.data = reduce(lambda left, right: pd.merge(left, right, left_index=True, right_index=True),
                           [data_provider.data for data_provider in data_providers if data_provider.data is not None])
