import pandas as pd

from irl.data.data_manipulator.base import TickerDataManipulator


class CCIDataManipulator(TickerDataManipulator):
    def __init__(self, period=20):
        super().__init__()
        self.period = period

    def calculate(self,
                  data: pd.DataFrame) -> pd.DataFrame:
        """
        calculates cci and append it to the dataframe

        :param data: initial pandas dataframe
        :return: pandas dataframe with cci column added
        """

        typical_price = (data['High'] + data['Low'] + data['Close']) / 3
        numerator = typical_price - pd.rolling_mean(typical_price, self.period)
        denominator = (0.015 * pd.rolling_std(typical_price, self.period))
        data['CCI'] = numerator / denominator
        return data
