from typing import List

import pandas as pd

from irl.data.data_manipulator.base import TickerDataManipulator


class MovingAverageDataManipulator(TickerDataManipulator):
    def __init__(self,
                 periods: List[int]):
        """
        :param data_provider: base data provided by yfinance
        :param periods: list of period to calculate ma over
        """
        super().__init__()
        self.periods = periods

    def calculate(self,
                  data: pd.DataFrame) -> pd.DataFrame:
        """
        calculates moving averages and append it to the dataframe

        :param data: initial pandas dataframe
        :return: pandas dataframe with moving average columns added
        """
        for period in self.periods:
            ma = data['Close'].rolling(period).mean()
            data['SMA_' + str(period)] = ma
        return data
