import pandas as pd

from irl.data.data_manipulator.base import TickerDataManipulator


class SCIDataManipulator(TickerDataManipulator):
    def __init__(self, period=14):
        super().__init__()
        self.period = period

    def calculate(self,
                  data: pd.DataFrame) -> pd.DataFrame:
        """
        calculates sci and append it to the dataframe

        :param data: initial pandas dataframe
        :return: pandas dataframe with sci column added
        """

        l14 = data['Low'].rolling(self.period).min()
        h14 = data['High'].rolling(self.period).max()
        data['SCI'] = (data['Close'] - l14) / (h14 - l14) * 100
        return data
