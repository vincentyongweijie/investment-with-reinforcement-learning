import numpy as np
import pandas as pd

from irl.data.data_manipulator.base import TickerDataManipulator


class ADXDataManipulator(TickerDataManipulator):
    def __init__(self, period=14):
        super().__init__()
        self.period = period

    def calculate(self,
                  data: pd.DataFrame) -> pd.DataFrame:
        """
        calculates adx and append it to the dataframe
        https://stackoverflow.com/questions/63020750/how-to-find-average-directional-movement-for-stocks-using-pandas

        :param data: initial pandas dataframe
        :return: pandas dataframe with adx column added
        """
        alpha = 1 / self.period

        # TR
        high_low_dif = data['High'] - data['Low']
        high_close_diff = np.abs(data['High'] - data['Close'].shift(1))
        low_close_diff = np.abs(data['Low'] - data['Close'].shift(1))
        tr = pd.concat([high_low_dif, high_close_diff, low_close_diff], axis=1).max(axis=1)

        # ATR
        atr = tr.ewm(alpha=alpha, adjust=False).mean()

        # +-DX
        high_prevhigh_diff = data['High'] - data['High'].shift(1)
        prevlow_low_diff = data['Low'].shift(1) - data['Low']
        positive_dx = np.where(
            (high_prevhigh_diff > prevlow_low_diff) & (high_prevhigh_diff > 0),
            high_prevhigh_diff,
            0.0
        )
        negative_dx = np.where(
            (high_prevhigh_diff < prevlow_low_diff) & (prevlow_low_diff > 0),
            prevlow_low_diff,
            0.0
        )

        # +- DMI
        positive_dm = positive_dx.ewm(alpha=alpha, adjust=False).mean()
        negative_dm = negative_dx.ewm(alpha=alpha, adjust=False).mean()
        positive_dmi = (positive_dm / atr) * 100
        negative_dmi = (negative_dm / atr) * 100

        # ADX
        dx = (np.abs(positive_dmi - negative_dmi) / (positive_dmi + negative_dmi)) * 100
        data['ADX'] = dx.ewm(alpha=alpha, adjust=False).mean()

        return data
