import numpy as np

import pandas as pd

from irl.data.data_manipulator.base import TickerDataManipulator


class OBVDataManipulator(TickerDataManipulator):
    def __init__(self):
        """
        :param data_provider: base data provided by yfinance
        """
        super().__init__()

    def calculate(self,
                  data: pd.DataFrame) -> pd.DataFrame:
        """
        calculates obv and append it to the dataframe
        https://stackoverflow.com/questions/52671594/calculating-stockss-on-balance-volume-obv-in-python

        :param data: pandas dataframe with 'Close' prices and 'Volume'
        :return: pandas dataframe with obv column added
        """
        data['OBV'] = (np.sign(data['Close'].diff()) * data['Volume']).fillna(0).cumsum()
        return data
