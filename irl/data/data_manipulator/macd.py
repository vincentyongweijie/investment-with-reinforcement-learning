import pandas as pd

from irl.data.data_manipulator.base import TickerDataManipulator


class MACDDataManipulator(TickerDataManipulator):
    def __init__(self):
        """
        :param data_provider: base data provided by yfinance
        """
        super().__init__()

    def calculate(self,
                  data: pd.DataFrame) -> pd.DataFrame:
        """
        calculates macd and append it to the dataframe

        :param data: initial pandas dataframe
        :return: pandas dataframe with macd column added
        """
        k = data['Close'].ewm(span=12, adjust=False, min_periods=12).mean()
        d = data['Close'].ewm(span=26, adjust=False, min_periods=26).mean()
        data['macd'] = k - d
        data['macd_s'] = data['macd'].ewm(span=9, adjust=False, min_periods=9).mean()
        data['macd_h'] = data['macd'] - data['macd_s']
        return data
