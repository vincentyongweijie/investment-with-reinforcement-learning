import pandas as pd

from irl.data.data_manipulator.base import TickerDataManipulator


class RSIDataManipulator(TickerDataManipulator):
    def __init__(self,
                 period: int = 14,
                 ema: bool = True):
        """
        :param data_provider: base data provided by yfinance
        :param periods: period to calculate rsi
        :param ema: whether to use exponential or simple moving average
        """
        super().__init__()
        self.period = period
        self.ema = ema

    def calculate(self,
                  data: pd.DataFrame) -> None:
        """
        calculates rsi and append it to the dataframe
        https://www.roelpeters.be/many-ways-to-calculate-the-rsi-in-python-pandas/

        :param data: pandas dataframe with 'Close' prices
        :return: pandas dataframe with rsi column added
        """
        close_delta = data['Close'].diff()
        up = close_delta.clip(lower=0)
        down = -1 * close_delta.clip(upper=0)

        if self.ema:
            ma_up = up.ewm(com=self.period - 1, min_periods=self.period).mean()
            ma_down = down.ewm(com=self.period - 1, min_periods=self.period).mean()
        else:
            ma_up = up.rolling(window=self.period).mean()
            ma_down = down.rolling(window=self.period).mean()

        rsi = ma_up / ma_down
        rsi = 100 - (100 / (1 + rsi))
        column_name = 'RSI_' + ('EMA' if self.ema else 'SMA') + '_' + str(self.period)
        data[column_name] = rsi
        return data
