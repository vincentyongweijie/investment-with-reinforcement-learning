from abc import abstractmethod, ABCMeta

import pandas as pd

from irl.data.data_combinator.base import TickerDataCombinator, GeneralDataCombinator, BaseDataCombinator


class BaseDataManipulator(metaclass=ABCMeta):
    """
    base class to manipulate data from data providers
    """
    def __init__(self):
        pass

    @abstractmethod
    def manipulate(self,
                   data_combinator: BaseDataCombinator) -> None:
        """

        :param data_combinator: contains combined data across data providers
        :return: returns the dataframe with the necessary fields calculated
        """

    @abstractmethod
    def calculate(self,
                  data: pd.DataFrame) -> pd.DataFrame:
        """

        :param data: initial dataframe
        :return: returns dataframe with new column added
        """


class TickerDataManipulator(BaseDataManipulator, metaclass=ABCMeta):
    def __init__(self):
        super().__init__()

    def manipulate(self, data_combinator: TickerDataCombinator) -> None:
        for ticker, ticker_data in data_combinator.data.items():
            data_combinator.data[ticker] = self.calculate(ticker_data)


class GeneralDataManipulator(BaseDataManipulator, metaclass=ABCMeta):
    def __init__(self):
        super().__init__()

    def manipulate(self, data_combinator: GeneralDataCombinator) -> None:
        data_combinator.data = self.calculate(data_combinator.data)
