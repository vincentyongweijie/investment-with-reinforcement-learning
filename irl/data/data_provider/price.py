# import concurrent
import datetime
import requests
import time
from typing import List, Dict, Any

import pandas as pd

from irl.data.data_provider.base import TickerDataProvider


class TickerPriceDataProvider(TickerDataProvider):
    def __init__(self,
                 tickers: List[str]):
        self.url = 'https://query1.finance.yahoo.com/v8/finance/chart/{0}?symbol={0}' \
                   '&period1=0&period2={1}&useYfid=true&interval=1d&includePrePost=true' \
                   '&events=div%7Csplit%7Cearn&lang=en-US&region=US&crumb=SdG7arzbKRl' \
                   '&corsDomain=finance.yahoo.com'
        self.headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
                                      'AppleWebKit/537.36 (KHTML, like Gecko) '
                                      'Chrome/96.0.4664.110 Safari/537.36'}
        super().__init__(tickers)

    def create_dataframe(self, response: Dict[str, Any]) -> pd.DataFrame:
        response = response['chart']['result'][0]
        timestamps = response['timestamp']
        indices = [datetime.datetime.fromtimestamp(timestamp) for timestamp in timestamps]

        prices = response['indicators']['quote'][0]
        open_prices = prices['open']
        close_prices = prices['close']
        high_prices = prices['high']
        low_prices = prices['low']
        volume = prices['volume']

        df = pd.DataFrame(index=['Open', 'Close', 'High', 'Low', 'Volume'],
                          data=[open_prices, close_prices, high_prices, low_prices, volume],
                          columns=indices)
        return df.T

    def get_ticker_data(self, ticker_name: str) -> None:
        url = self.url.format(ticker_name, int(time.time()))
        response = requests.get(url, headers=self.headers)
        self.data[ticker_name] = self.create_dataframe(response.json())

    def get_data(self) -> None:
        # with concurrent.futures.ThreadPoolExecutor() as executor:
        #     for ticker_name in self.tickers:
        #         executor.submit(self.get_ticker_data, ticker_name=ticker_name)

        for ticker_name in self.tickers:
            self.get_ticker_data(ticker_name)
