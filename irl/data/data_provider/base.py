from abc import abstractmethod, ABCMeta
from collections import OrderedDict
from typing import List


class BaseDataProvider(metaclass=ABCMeta):
    def __init__(self):
        pass

    @abstractmethod
    def get_data(self) -> None:
        """
        to be implemented by sub-classes

        sub-classes decide which datasets to retrieve
        the data is then stored as self.data[ticker] as a pandas dataframe for ticker data
        or just stored as self.data as a pandas dataframe for general data
        """


class TickerDataProvider(BaseDataProvider, metaclass=ABCMeta):
    def __init__(self,
                 tickers: List[str]):
        """
        :param tickers: ticker symbols to get data for
        """
        self.tickers = tickers
        self.data = OrderedDict()


class GeneralDataProvider(BaseDataProvider, metaclass=ABCMeta):
    def __init__(self):
        super().__init__()
        self.data = None
