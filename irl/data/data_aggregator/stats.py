import numpy as np
import pandas as pd

from scipy.spatial import distance

from irl.data.data_aggregator.base import BaseAggregatorStats
from irl.data.data_combinator.base import TickerDataCombinator


class TurbulenceIndex(BaseAggregatorStats):
    def __init__(self):
        pass

    def calculate(self,
                  ticker_data_combinator: TickerDataCombinator) -> pd.DataFrame:
        close_prices = self.get_column(ticker_data_combinator, 'Close')
        dates = close_prices.index
        close_prices = close_prices.values
        returns = pd.DataFrame((close_prices[1:] - close_prices[:-1]) / close_prices[:-1] * 100)

        window_size = 250
        curr_index = window_size
        turbulence_values = []
        while curr_index < len(returns):
            historical_sample = returns.iloc[curr_index-window_size:curr_index]
            historical_sample_means = historical_sample.mean()
            inverse_covariance_matrix = np.linalg.pinv(historical_sample.cov())
            current_data = returns.iloc[curr_index]

            mahalanobis_distance = distance.mahalanobis(u=current_data,
                                                        v=historical_sample_means,
                                                        VI=inverse_covariance_matrix)
            turbulence_values.append(mahalanobis_distance ** 2)
            curr_index += 1

        turbulence_values = [0] * (close_prices.shape[0] - len(turbulence_values)) + turbulence_values
        return pd.DataFrame(turbulence_values, columns=['turbulence_index'], index=dates)


class SystemicRiskIndex(BaseAggregatorStats):
    def __init__(self):
        super().__init__()

    def gini(self, values: np.ndarray) -> float:
        values = list(values)
        values.sort()

        minimum_value = values[0]
        lorenz_curve_value = minimum_value
        average_input = sum(values) / len(values)
        line_of_equality = [average_input]
        gap_area = [line_of_equality[0] - lorenz_curve_value]

        for index in range(1, len(values)):
            lorenz_curve_value += values[index]
            line_of_equality.append(line_of_equality[index - 1] + average_input)
            gap_area.append(line_of_equality[index - 1] + average_input
                            - lorenz_curve_value)

        return sum(gap_area) / sum(line_of_equality)

    def calculate(self,
                  ticker_data_combinator: TickerDataCombinator) -> pd.DataFrame:
        close_prices = self.get_column(ticker_data_combinator, 'Close')
        dates = close_prices.index
        close_prices = close_prices.values
        returns = pd.DataFrame((close_prices[1:] - close_prices[:-1]) / close_prices[:-1] * 100)

        window_size = 250
        curr_index = window_size
        systemic_risk_values = []
        while curr_index < len(returns):
            covariance_matrix = returns.iloc[curr_index + 1 - window_size:curr_index + 1].cov()
            eigenvalues = np.sort(np.linalg.eig(covariance_matrix)[0])
            systemic_risk_values.append(self.gini(values=eigenvalues))
            curr_index += 1

        systemic_risk_values = [0] * (close_prices.shape[0] - len(systemic_risk_values)) + systemic_risk_values
        return pd.DataFrame(systemic_risk_values, columns=['systemic_risk'], index=dates)
