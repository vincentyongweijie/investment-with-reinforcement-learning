from abc import ABCMeta, abstractmethod
import bisect
import datetime
from typing import List, Dict, Union

import numpy as np
import pandas as pd

from irl.data.data_aggregator.normalisers import normalisers
from irl.data.data_combinator.base import TickerDataCombinator, GeneralDataCombinator


class BaseAggregationCheck(metaclass=ABCMeta):
    def __init__(self,
                 name: str,
                 error_message: str):
        self.name = name
        self.error_message = error_message
        pass

    @abstractmethod
    def check(self, dataframes: List[pd.DataFrame]) -> bool:
        """
        sub classes determine checks to be carried out

        :param dataframes: all dataframes to be checked
        :return: boolean indicating whether the check passed or failed
        """


class BaseAggregatorStats:
    def __init__(self):
        pass

    def get_column(self,
                   ticker_data_combinator: TickerDataCombinator,
                   column: str):
        values = [ticker_data_combinator.data[ticker][column] for ticker in ticker_data_combinator.data]
        return pd.concat(values, axis=1)

    @abstractmethod
    def calculate(self,
                  ticker_data_combinator: TickerDataCombinator) -> np.ndarray:
        """
        sub classes determine how to calculate stats

        :param ticker_data_combinator: TickerDataCombinator instance
        :return: boolean indicating whether the check passed or failed
        """

class BaseDataAggregator:
    """
    base class to aggregate data across multiple tickers
    """
    def __init__(self,
                 period: int,
                 start_date: str,
                 end_date: str,
                 val_start_date: str,
                 val_end_date: str,
                 ticker_data_columns: List[str],
                 general_data_columns: List[str],
                 checks: List[BaseAggregationCheck],
                 ticker_normalising_rules: Dict,
                 general_normalising_rules: Dict,
                 aggregator_stats: List[BaseAggregatorStats]):
        self.period = period
        self.start_date = datetime.datetime.strptime(start_date, '%Y%m%d')
        self.end_date = datetime.datetime.strptime(end_date, '%Y%m%d')
        self.val_start_date = datetime.datetime.strptime(val_start_date, '%Y%m%d')
        self.val_end_date = datetime.datetime.strptime(val_end_date, '%Y%m%d')

        self.ticker_data_columns = ticker_data_columns
        self.general_data_columns = general_data_columns
        self.data = None
        self.open_prices = None

        self.checks = checks

        self.ticker_normalising_rules = ticker_normalising_rules
        self.general_normalising_rules = general_normalising_rules

        self.aggregator_stats = aggregator_stats

    def init_prices(self,
                    ticker_data_combinator: TickerDataCombinator,
                    price_type: str) -> Union[np.ndarray, np.ndarray]:
        prices = []
        val_prices = []

        for ticker in ticker_data_combinator.data:
            prices.append(self.filter_by_date(
                ticker_data_combinator.data[ticker][price_type],
                validation=False
            ).rename(ticker + ' ' + price_type))

            val_prices.append(self.filter_by_date(
                ticker_data_combinator.data[ticker][price_type],
                validation=True
            ).rename(ticker + ' ' + price_type))

        prices = pd.concat(prices, axis=1)[self.period:]
        val_prices = pd.concat(val_prices, axis=1)[self.period:]
        return prices, val_prices

    def get_tickers_data(self,
                         ticker_data_combinator: TickerDataCombinator) -> Union[np.ndarray, np.ndarray]:
        tickers_data = []
        val_tickers_data = []

        for ticker in ticker_data_combinator.data:
            tickers_data.append(self.make_3d_array(
                self.filter_by_date(
                    ticker_data_combinator.data[ticker][self.ticker_data_columns],
                    validation=False
                ),
                self.ticker_normalising_rules))

            val_tickers_data.append(self.make_3d_array(
                self.filter_by_date(
                    ticker_data_combinator.data[ticker][self.ticker_data_columns],
                    validation=True
                ),
                self.ticker_normalising_rules))

        tickers_data = np.concatenate(tickers_data, axis=-1)
        val_tickers_data = np.concatenate(val_tickers_data, axis=-1)
        return tickers_data, val_tickers_data

    def get_general_data(self,
                         general_data_combinator: GeneralDataCombinator) -> Union[np.ndarray, np.ndarray]:
        general_data = None
        val_general_data = None
        if general_data_combinator and general_data_combinator.data:
            general_data = self.make_3d_array(
                self.filter_by_date(
                    general_data_combinator.data[self.general_data_columns],
                    validation=False
                ),
                self.general_normalising_rules)

            val_general_data = self.make_3d_array(
                self.filter_by_date(
                    general_data_combinator.data[self.general_data_columns],
                    validation=True
                ),
                self.general_normalising_rules)
        return general_data, val_general_data

    def aggregate(self,
                  ticker_data_combinator: TickerDataCombinator,
                  general_data_combinator: GeneralDataCombinator) -> None:

        # if not self.pass_all_checks(ticker_data_combinator,
        #                             general_data_combinator):
        #     return

        self.open_prices, self.val_open_prices = self.init_prices(ticker_data_combinator, 'Open')
        self.close_prices, self.val_close_prices = self.init_prices(ticker_data_combinator, 'Close')
        tickers_data, val_tickers_data = self.get_tickers_data(ticker_data_combinator)
        general_data, val_general_data = self.get_general_data(general_data_combinator)

        self.data = np.concatenate([tickers_data, general_data], axis=-1) \
            if general_data \
            else tickers_data
        self.val_data = np.concatenate([val_tickers_data, val_general_data], axis=-1) \
            if val_general_data \
            else val_tickers_data

        # aggregator_stats = [aggregator_stat.calculate(ticker_data_combinator) for aggregator_stat in self.aggregator_stats]
        # aggregator_stats_df = pd.concat(aggregator_stats, axis=1)
        #
        # self.data = np.concatenate(data, self.filter_by_date(
        #     aggregator_stats_df,
        #     validation=False
        # ), axis=-1)
        # self.val_data = np.concatenate(val_data, self.filter_by_date(
        #     aggregator_stats_df,
        #     validation=True
        # ), axis=-1)

    def pass_all_checks(self,
                        ticker_data_combinator: TickerDataCombinator,
                        general_data_combinator: GeneralDataCombinator) -> bool:
        dfs = [ticker_data_combinator.data[ticker] for ticker in ticker_data_combinator.data]
        if general_data_combinator and general_data_combinator.data:
            dfs += general_data_combinator.data

        for check in self.checks:
            if not check.check(dfs):
                print(check.name, 'FAILED:', check.error_message)
                return False
        return True

    def filter_by_date(self,
                       data: pd.DataFrame,
                       validation: bool) -> pd.DataFrame:
        start_date_idx = bisect.bisect_left(data.index, self.val_start_date if validation else self.start_date)
        start_idx = start_date_idx - self.period
        end_date_idx = bisect.bisect_right(data.index, self.val_end_date if validation else self.end_date)
        return data[start_idx:end_date_idx]

    def scale(self,
              data: np.ndarray,
              curr_min: float,
              curr_max: float,
              final_min: float = -1,
              final_max: float = 1) -> np.ndarray:
        data -= curr_min
        curr_max -= curr_min

        data /= curr_max
        data *= (final_max - final_min)
        data += final_min
        return data

    def make_3d_array(self,
                      data: pd.DataFrame,
                      normalising_rules: Dict) -> np.ndarray:
        length = len(data)
        num_columns = len(data.columns)
        array = np.zeros((length-self.period, self.period, num_columns))

        for row in range(length-self.period):
            tmp_data = data[row:row+self.period].copy()

            for normalising_column, normalising_params in normalising_rules.items():
                columns_to_be_normalised = normalising_params[0]
                normalising_type = normalising_params[1]
                scaling_kwargs = normalising_params[2]

                if normalising_type:
                    normaliser = normalisers[normalising_type]

                    tmp_data.loc[:, columns_to_be_normalised] = normaliser.fit_and_transform(
                        data_fitted_on=tmp_data.loc[:, [normalising_column]].values,
                        data_to_be_normalised=tmp_data.loc[:, columns_to_be_normalised].values
                    )

                tmp_data.loc[:, columns_to_be_normalised] = self.scale(tmp_data.loc[:, columns_to_be_normalised],
                                                                       **scaling_kwargs)

            array[row, :, :] = tmp_data.values
        return array
