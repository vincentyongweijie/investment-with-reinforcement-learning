from abc import ABCMeta

import numpy as np
from sklearn.preprocessing import MinMaxScaler, StandardScaler, MaxAbsScaler


class BaseNormaliser(metaclass=ABCMeta):
    def __init__(self):
        self.normaliser = None

    def fit_and_transform(self,
                          data_fitted_on: np.ndarray,
                          data_to_be_normalised: np.ndarray) -> np.ndarray:
        self.normaliser.fit(data_fitted_on)
        for i in range(data_to_be_normalised.shape[1]):
            normalised_data = self.normaliser.transform(data_to_be_normalised[:, i].reshape(-1, 1))
            data_to_be_normalised[:, i] = normalised_data.flatten()
        return data_to_be_normalised


class MinMaxNormaliser(BaseNormaliser):
    def __init__(self):
        super().__init__()
        self.normaliser = MinMaxScaler()


class StandardNormaliser(BaseNormaliser):
    def __init__(self):
        super().__init__()
        self.normaliser = StandardScaler()


class MaxAbsNormaliser(BaseNormaliser):
    def __init__(self):
        super().__init__()
        self.normaliser = MaxAbsScaler()


normalisers = {
    "MinMax": MinMaxNormaliser(),
    "Standard": StandardNormaliser(),
    "MaxAbs": MaxAbsNormaliser()
}
