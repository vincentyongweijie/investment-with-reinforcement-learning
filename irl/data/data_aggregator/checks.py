from irl.data.data_aggregator.base import BaseAggregationCheck


class SameDateRangeCheck(BaseAggregationCheck):
    def __init__(self,
                 name='SameDateRangeCheck',
                 error_message='Some dataframe(s) for not have same dates'):
        super().__init__(name, error_message)

    def check(self, dataframes):
        return all((df1.index == df2.index).all() for df1, df2, in zip(dataframes, dataframes[1:]))


class NoNanCheck(BaseAggregationCheck):
    def __init__(self,
                 name='NoNanCheck',
                 error_message='There are NaN values in some of the dataframe(s)'):
        super().__init__(name, error_message)

    def check(self, dataframes):
        return all(not df.isnull().values.any() for df in dataframes)
