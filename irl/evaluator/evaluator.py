from glob import glob
import os

import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
import pandas as pd


class Evaluator:
    def __init__(self):
        self.writer = animation.FFMpegWriter(fps=10,
                                             metadata=dict(artist='Me'),
                                             bitrate=1800)

    def visualise_performance(self,
                              max_return: int,
                              max_val: int,
                              output_dir: str):
        self.plot_training_portfolios(max_val, output_dir)
        self.plot_training_returns(max_return, output_dir)

    def get_axes(self,
                 num_envs: int,
                 rows: int):
        cols = num_envs // rows
        fig, ax = plt.subplots(rows, cols, figsize=(20, 20))

        return fig, ax, rows, cols

    def plot_training_portfolios(self,
                                 max_val: int,
                                 output_dir: str) -> None:

        num_envs = len(os.listdir(output_dir))

        fig, ax, rows, cols = self.get_axes(num_envs, 2)
        ax.set_ylim(0, max_val)

        lines = []
        for env_num in range(num_envs):
            env_name = ' env_' + str(env_num + 1)
            df = pd.read_csv(os.path.join(output_dir, env_name, '1.csv'), index_col=0)
            line, = ax[env_num // cols][env_num % rows].plot(range(len(df)), df['Total Value'])
            lines.append(line)

        def animate(i):
            for env_num in range(num_envs):
                env_name_ = ' env_' + str(env_num + 1)
                df_ = pd.read_csv(os.path.join(output_dir, env_name_, str(i) + '.csv'), index_col=0)
                lines[env_num].set_ydata(df_['Total Value'])  # update the data
            return lines,

        def init():
            for line in lines:
                line.set_ydata(np.ma.array(df['Total Value'], mask=True))
            return lines,

        length = len(glob(os.path.join(output_dir, 'env_1', '*.csv')))
        ani = animation.FuncAnimation(fig,
                                      animate,
                                      np.arange(1, length),
                                      init_func=init,
                                      interval=10,
                                      blit=True)

        ani.save(os.path.join(output_dir, 'performance.mp4'),
                 writer=self.writer)

    def plot_training_returns(self,
                              max_return: int,
                              output_dir: str):

        num_envs = len(os.listdir(output_dir))

        fig, ax, rows, cols = self.get_axes(num_envs, 2)
        ax.set_ylim(0, max_return)

        for env_num in range(num_envs):
            env_name = ' env_' + str(env_num + 1)
            returns = []
            for iteration in range(len(glob(os.path.join(output_dir, env_name, '*.csv')))):
                df = pd.read_csv(os.path.join(output_dir, env_name, str(iteration+1), '.csv'), index_col=0)
                values = df['Total Value']
                returns.append((values[-1] - values[0]) / values[0] * 100)
            ax[env_num // cols][env_num % rows].plot(range(len(df)), df['Total Value'])
        plt.savefig(os.path.join(output_dir, 'returns.png'))
