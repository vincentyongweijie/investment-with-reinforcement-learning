from abc import ABC
import os
from typing import List

import datetime
import numpy as np
import pandas as pd


class PortfolioHistory(pd.DataFrame, ABC):
    def __init__(self, tickers: List[str]):
        position_columns = ['Num Shares of ' + ticker for ticker in tickers]
        prices_columns = ['Close Price of ' + ticker for ticker in tickers]
        super().__init__(columns=prices_columns + position_columns + ['Total Value'])

    def add_row(self,
                date: datetime.datetime,
                close_prices: np.ndarray,
                positions: np.ndarray,
                total_val: float):
        self.loc[date] = np.append(np.concatenate((close_prices, positions)), total_val)


class Portfolio:
    def __init__(self,
                 period: int,
                 max_power: int,
                 tickers: List[str],
                 total_val: float,
                 output_dir: str):
        """
        :param tickers: tickers to trade
        :param total_val: initial value of portfolio to start with
        """
        self.period = period
        self.max_power = max_power
        self.transaction_cost_percentage = 0.2 / 100
        self.output_dir = output_dir
        self.squarer = lambda t: 2 ** t if t > 0 else (0 if t == 0 else -(2 ** -t))

        self.tickers = tickers + ['USD']
        self.num_instruments = len(self.tickers)
        self.initial_val = total_val

        self.history_idx = -1
        self.history = PortfolioHistory(self.tickers)

        self.reset()

    def reset(self) -> np.ndarray:
        if self.history_idx > 0:
            self.history.to_csv(os.path.join(self.output_dir, str(self.history_idx) + '.csv'))
            print(self.output_dir, self.total_val/self.initial_val * 100, self.cash_penalty)
        self.history_idx += 1
        self.history = PortfolioHistory(self.tickers)

        self.positions = np.array([0] * self.num_instruments)
        self.positions[-1] = self.initial_val
        self.total_val = self.initial_val

        self.returns = 0.0
        self.cash_penalty = 0.0
        self.reward = 0.0

        self.state = np.zeros((self.period, self.num_instruments))
        return self.state

    def add_curr_state_to_history(self,
                                  date: datetime.datetime,
                                  close_prices: np.ndarray):
        """
        adds row to portfolio history with internal parameters
        """
        self.history.add_row(date=date,
                             close_prices=close_prices,
                             positions=self.positions,
                             total_val=self.total_val)

    def execute_initial_action(self,
                               action: np.ndarray):
        """
        plans action without considerng buy_all action

        :param action:
        :return:
        """
        num_shares_to_buy_or_sell = (action - self.max_power).astype(int)
        num_shares_to_buy_or_sell = np.vectorize(self.squarer)(num_shares_to_buy_or_sell)
        # buy_all_mask = num_shares_to_buy_or_sell == 2 ** self.max_power

        # adjust those which are selling more than owned
        num_shares_to_buy_or_sell = np.where(num_shares_to_buy_or_sell < -self.positions[:-1],
                                             -self.positions[:-1],
                                             num_shares_to_buy_or_sell)
        # let left/right border be sell all action
        num_shares_to_buy_or_sell = np.where((num_shares_to_buy_or_sell == 2 ** self.max_power) |
                                             (num_shares_to_buy_or_sell == -(2 ** self.max_power)),
                                             -self.positions[:-1],
                                             num_shares_to_buy_or_sell)

        # ignore buy all action first
        # num_shares_to_buy_or_sell = np.where((num_shares_to_buy_or_sell == 2 ** self.max_power),
        #         #                                      0,
        #         #                                      num_shares_to_buy_or_sell)
        #         # return num_shares_to_buy_or_sell, buy_all_mask
        return num_shares_to_buy_or_sell

    def execute(self,
                date: datetime.datetime,
                action: np.ndarray,
                next_day_open_prices: np.ndarray,
                next_day_close_prices: np.ndarray):
        """
        to be invoked at the end of the trading day

        checks whether we can execute the action and executes it if possible
        :param action: decision given by RL algorithm
        :param next_day_open_prices: closing prices
        :return:
        """
        num_shares_to_buy_or_sell = self.execute_initial_action(action)
        amt_needed_per_stock = num_shares_to_buy_or_sell * next_day_open_prices

        gain_ratios = np.array([1.0] * (self.num_instruments - 1))
        amt_needed = np.sum(amt_needed_per_stock * gain_ratios)

        gain_ratios[amt_needed > 0] = 1 + self.transaction_cost_percentage
        gain_ratios[amt_needed < 0] = 1 - self.transaction_cost_percentage
        amt_needed_with_fees = np.sum(amt_needed_per_stock * gain_ratios)  # exclude the currency

        next_positions = np.append(self.positions[:-1] + num_shares_to_buy_or_sell,
                                   self.positions[-1] - amt_needed_with_fees)

        if next_positions[-1] >= 0:
            # let right border be buy all action
            # total_buy_all_price = np.sum(next_day_open_prices[buy_all_mask]) * (1 + self.transaction_cost_percentage)
            # if total_buy_all_price > 0:
            #     num_buy_all_shares = int(next_positions[-1] / total_buy_all_price)
            #     next_positions[:-1][buy_all_mask] = num_buy_all_shares
            #     next_positions[-1] -= total_buy_all_price * num_buy_all_shares
            self.positions = next_positions

        next_day_open_prices = np.append(next_day_open_prices, 1)
        next_day_close_prices = np.append(next_day_close_prices, 1)
        open_val = np.sum(self.positions * next_day_open_prices)
        self.state = np.roll(self.state, -1, axis=0)
        self.state[-1] = (((self.positions * next_day_open_prices) / open_val) - 0.5) * 2

        prev_val = self.total_val
        self.total_val = np.sum(self.positions * next_day_close_prices)
        self.returns = (self.total_val - prev_val) / prev_val * 100
        self.cash_penalty = (self.initial_val - self.positions[-1]) / self.initial_val
        self.reward = self.returns + self.cash_penalty
        self.add_curr_state_to_history(date, next_day_close_prices)
