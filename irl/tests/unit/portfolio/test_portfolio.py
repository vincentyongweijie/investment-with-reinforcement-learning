import datetime
import numpy as np
import pandas as pd

from irl.portfolio.portfolio import Portfolio, PortfolioHistory

date = datetime.datetime.now()
tickers = ['AMD', 'AAPL', 'GOOG']


def test_portfolio_history_instantiated_correctly():
    position_columns = ['Num Shares of ' + ticker for ticker in tickers]
    prices_columns = ['Close Price of ' + ticker for ticker in tickers]
    df = pd.DataFrame(columns=prices_columns + position_columns + ['Total Value'])

    history = PortfolioHistory(tickers=tickers)
    assert df.equals(history)


def test_portfolio_history_rows_added_correctly():
    position_columns = ['Num Shares of ' + ticker for ticker in tickers + ['USD']]
    prices_columns = ['Close Price of ' + ticker for ticker in tickers + ['USD']]
    df = pd.DataFrame(columns=prices_columns + position_columns + ['Total Value'])
    df.loc[date] = [0.2, 0.3, 0.1, 0.4, 1, 2, 3, 4, 1e5]

    history = PortfolioHistory(tickers=tickers + ['USD'])
    history.add_row(date, np.array([0.2, 0.3, 0.1, 0.4]), np.array([1, 2, 3, 4]), 1e5)
    assert df.equals(history)


def test_portfolio_executes_correctly():
    position_columns = ['Num Shares of ' + ticker for ticker in tickers + ['USD']]
    prices_columns = ['Close Price of ' + ticker for ticker in tickers + ['USD']]
    df = pd.DataFrame(columns=prices_columns + position_columns + ['Total Value'])

    portfolio = Portfolio(period=10,
                          max_power=16,
                          tickers=tickers,
                          total_val=1e6,
                          output_dir='./')
    next_day_open_prices = np.array([50, 200, 300])
    next_day_close_prices = np.array([50, 200, 300])

    # buy 4 AMD, 16 AAPL
    next_action = np.array([18, 20, 0])
    portfolio.execute(date - datetime.timedelta(days=1),
                      next_action,
                      next_day_open_prices,
                      next_day_close_prices)
    df.loc[date - datetime.timedelta(days=1)] = [50, 200, 300, 1, 4, 16, 0, 1e6 - 3406.8, 1e6 - 6.8]
    assert np.isclose(df.iloc[-1], portfolio.history.iloc[-1]).all()

    # sell 2 AMD, do nothing for AAPL
    next_action = np.array([15, 16, 0])
    portfolio.execute(date,
                      next_action,
                      next_day_open_prices,
                      next_day_close_prices)
    df.loc[date] = [50, 200, 300, 1, 2, 16, 0, 1e6 - 3406.8 + 99.8, 1e6 - 6.8 - 0.2]
    assert np.isclose(df.iloc[-1], portfolio.history.iloc[-1]).all()

    # do nothing for AMD, sell extra for AAPL
    next_action = np.array([16, 8, 0])
    portfolio.execute(date + datetime.timedelta(days=1),
                      next_action,
                      next_day_open_prices,
                      next_day_close_prices)
    df.loc[date + datetime.timedelta(days=1)] = [50, 200, 300, 1, 2, 0, 0,
                                                 1e6 - 3406.8 + 99.8 + 3193.6,
                                                 1e6 - 6.8 - 0.2 - 6.4]
    assert np.isclose(df.iloc[-1], portfolio.history.iloc[-1]) .all()

    # buy all GOOG
    # next_action = np.array([16, 16, 32])
    # portfolio.execute(date + datetime.timedelta(days=2),
    #                   next_action,
    #                   next_day_open_prices,
    #                   next_day_close_prices)
    # df.loc[date + datetime.timedelta(days=2)] = [50, 200, 300, 1, 2, 0, 3326,
    #                                              1e6 - 3406.8 + 99.8 + 3193.6 - (300.6*3326),
    #                                              1e6 - 6.8 - 0.2 - 6.4 - 1995.6]
    # assert np.isclose(df.iloc[-1], portfolio.history.iloc[-1]).all()
