import numpy as np
import pandas as pd

from irl.data.data_provider.price import TickerPriceDataProvider

def test_get_correct_history():
    tickers = ['GOOG', 'AAPL', 'AMD']

    data_provider = TickerPriceDataProvider(tickers)
    data_provider.get_data()

    ground_truth = pd.read_csv('./irl/tests/resources/data/aapl_prices.csv', index_col=0)
    assert (data_provider.data['AAPL'].columns[:5] == ground_truth.columns[:5]).all()
    assert np.isclose(data_provider.data['AAPL'].iloc[:, :5],
                      ground_truth.iloc[:, :5]).all()
