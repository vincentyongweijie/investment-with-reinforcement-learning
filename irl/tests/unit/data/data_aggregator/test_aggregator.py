import numpy as np

from irl.data.data_aggregator.base import BaseDataAggregator
from irl.data.data_aggregator.checks import SameDateRangeCheck, NoNanCheck
from irl.data.data_combinator.base import TickerDataCombinator
from irl.data.data_manipulator.macd import MACDDataManipulator
from irl.data.data_manipulator.moving_average import MovingAverageDataManipulator
from irl.data.data_provider.price import TickerPriceDataProvider


def test_base_data_aggregator():
    tickers = ['GOOG', 'AAPL', 'AMD']

    data_provider = TickerPriceDataProvider(tickers)
    data_provider.get_data()

    data_combinator = TickerDataCombinator()
    data_combinator.combine([data_provider])

    data_manipulator = MovingAverageDataManipulator(periods=[5, 10, 20, 30])
    data_manipulator.manipulate(data_combinator)

    data_manipulator = MACDDataManipulator()
    data_manipulator.manipulate(data_combinator)

    data_aggregator = BaseDataAggregator(period=10,
                                         start_date='20110101',
                                         end_date='20201231',
                                         val_start_date='20210101',
                                         val_end_date='20211231',
                                         ticker_data_columns=["Close", "SMA_5", "SMA_10", "SMA_20", "SMA_30"],
                                         general_data_columns=[],
                                         checks=[SameDateRangeCheck(), NoNanCheck()],
                                         ticker_normalising_rules={
                                             "SMA_10": [["Close", "SMA_5", "SMA_10", "SMA_20", "SMA_30"],
                                                        "MaxAbs",
                                                        {"curr_min": 0, "curr_max": 2}],
                                         },
                                         general_normalising_rules={},
                                         aggregator_stats=None
                                         )
    data_aggregator.aggregate(data_combinator,
                              None)
    ground_truth = np.load('./irl/tests/resources/data/aggregated.npy')
    assert np.isclose(data_aggregator.data,
                      ground_truth,
                      rtol=1e-3,
                      atol=1e-3).all()
