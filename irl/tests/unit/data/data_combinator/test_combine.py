import numpy as np
import pandas as pd

from irl.data.data_provider.price import TickerPriceDataProvider
from irl.data.data_combinator.base import TickerDataCombinator


def test_correct_combination_of_data():
    tickers = ['GOOG', 'AAPL', 'AMD']

    data_provider = TickerPriceDataProvider(tickers)
    data_provider.get_data()

    data_combinator = TickerDataCombinator()
    data_combinator.combine([data_provider])

    ground_truth = pd.read_csv('./irl/tests/resources/data/aapl_prices.csv', index_col=0)
    assert (data_combinator.data['AAPL'].columns[:5] == ground_truth.columns[:5]).all()
    assert np.isclose(data_combinator.data['AAPL'].iloc[:, :5],
                      ground_truth.iloc[:, :5]).all()
