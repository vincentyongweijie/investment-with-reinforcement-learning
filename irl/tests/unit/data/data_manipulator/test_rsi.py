import numpy as np
import pandas as pd

from irl.data.data_combinator.base import TickerDataCombinator
from irl.data.data_provider.price import TickerPriceDataProvider
from irl.data.data_manipulator.rsi import RSIDataManipulator

def test_get_rsi_ema():
    tickers = ['GOOG', 'AAPL', 'AMD']

    data_provider = TickerPriceDataProvider(tickers)
    data_provider.get_data()

    data_combinator = TickerDataCombinator()
    data_combinator.combine([data_provider])

    data_manipulator = RSIDataManipulator()
    data_manipulator.manipulate(data_combinator)

    ground_truth = pd.read_csv('./irl/tests/resources/data/aapl_prices.csv', index_col=0)
    assert data_provider.data['AAPL'].columns[5:6] == ground_truth.columns[12:13]
    assert np.isclose(data_provider.data['AAPL'].iloc[:, 5],
                      ground_truth.iloc[:, 12],
                      equal_nan=True,
                      rtol=1e-3,
                      atol=1e-3).all()

def test_get_rsi_sma():
    tickers = ['GOOG', 'AAPL', 'AMD']

    data_provider = TickerPriceDataProvider(tickers)
    data_provider.get_data()

    data_combinator = TickerDataCombinator()
    data_combinator.combine([data_provider])

    data_manipulator = RSIDataManipulator(ema=False)
    data_manipulator.manipulate(data_combinator)

    ground_truth = pd.read_csv('./irl/tests/resources/data/aapl_prices.csv', index_col=0)
    assert data_provider.data['AAPL'].columns[5:6] == ground_truth.columns[13:14]
    assert np.isclose(data_provider.data['AAPL'].iloc[:, 5],
                      ground_truth.iloc[:, 13],
                      equal_nan=True,
                      rtol=1e-3,
                      atol=1e-3).all()
