import numpy as np
import pandas as pd

from irl.data.data_combinator.base import TickerDataCombinator
from irl.data.data_provider.price import TickerPriceDataProvider
from irl.data.data_manipulator.moving_average import MovingAverageDataManipulator

def test_get_moving_averages():
    tickers = ['GOOG', 'AAPL', 'AMD']

    data_provider = TickerPriceDataProvider(tickers)
    data_provider.get_data()

    data_combinator = TickerDataCombinator()
    data_combinator.combine([data_provider])

    data_manipulator = MovingAverageDataManipulator(periods=[5, 10, 20, 30])
    data_manipulator.manipulate(data_combinator)

    ground_truth = pd.read_csv('./irl/tests/resources/data/aapl_prices.csv', index_col=0)
    assert (data_provider.data['AAPL'].columns[5:9] == ground_truth.columns[5:9]).all()
    assert np.isclose(data_provider.data['AAPL'].iloc[:, 5:9],
                      ground_truth.iloc[:, 5:9],
                      equal_nan=True).all()
