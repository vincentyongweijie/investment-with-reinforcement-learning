from irl.env.stock_exchange import StockExchangeEnv
from irl.parser.config_parser import ConfigParser
from irl.data.data_combinator.base import TickerDataCombinator, GeneralDataCombinator
from irl.data.data_provider.base import BaseDataProvider
from irl.data.data_manipulator.base import BaseDataManipulator


def test_parser_instantiate_modules_correctly():
    parser = ConfigParser('./irl/tests/resources/parser/sector_config.json')
    assert isinstance(parser.ticker_data_combinator, TickerDataCombinator)
    assert isinstance(parser.general_data_combinator, GeneralDataCombinator)
    assert all(isinstance(data_provider, BaseDataProvider) for data_provider in parser.data_providers)
    assert all(isinstance(data_manipulator, BaseDataManipulator) for data_manipulator in parser.data_manipulators)

    assert isinstance(parser.initalise_env('1', False), StockExchangeEnv)
